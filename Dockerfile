FROM ubuntu:latest
#RUN apt-get install -y git \
#    apt-get install -y python3 \
#    apt-get install -y python3-pip
#RUN pip3 install Flask gunicorn
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y git
RUN apt-get install -y python3
RUN apt-get install -y python3-pip
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
WORKDIR /app
COPY . /app
